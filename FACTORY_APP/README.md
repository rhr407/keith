# PROJECT Secure Boot & Flash Encryption with OTA

This project is for testing the Secure Boot & Flash Encryption with OTA in esp32. The code here is the demonstration for AWS IoT thing MQTT messaging and OTA updates using AWS S3 bucket. 

## What you need?
In order to test this code, you need to have:

1. ESP32 dev-kit
2. AWS thing and AWS S3 setup in the AWS account. Both AWS thing and S3 bucket have been set up already. The thing name is `keith` and S3 bucket name is `keith-project`



## BEFORE ENCRYPTING THE PARTITIONS:

1. ```make menuconfig```  

2. Set the following:
a. Secure boot and flash encryption steps in menuconfig:

![Screenshot-20190605122946-554x225](https://user-images.githubusercontent.com/26026993/58953194-a6cb7180-878d-11e9-9585-6c8ac27e6c79.png)

b. Set two OTA partitions:

![Screenshot-20190605123202-557x142](https://user-images.githubusercontent.com/26026993/58953311-f6aa3880-878d-11e9-978c-7800da210f0a.png)

Above are the outputs in my case.

3. Do the Key Generation steps for secureboot and flash encryption putting the secure_boot_signing_key.pem and my_flash_encryption_key.bin in the project folder.

```
openssl ecparam -name prime256v1 -genkey -noout -out my_secure_boot_signing_key.pem
```

```
espsecure.py digest_private_key --keyfile secure_boot_signing_key.pem --keylen 256 my_flash_encryption_key.bin
```


## How to Test the Code??
1. Using terminal, go into the project folder and run the following command: 


```    
make menuconfig
```    

This opens a (blue background) menu on the terminal. 

2. Go to `WiFi and AWS Settings` and enter the WiFI SSID and PASSWORD of your WiFi router/mobile hotspot.

3. Make sure to set the `default serial port` in `Serial Flasher Config` according to your Operating System. In my case it's `/dev/ttyUSB0`

4. Leave the rest as it is.

5. Exit and save.

6. Showing the status of efuses:
```  
espefuse.py summary
```  
![Screenshot-20190610214200-733x705](https://user-images.githubusercontent.com/26026993/59225437-9e799900-8bc8-11e9-81d0-0221b7b7f31d.png)

7. The partition table offset is 0x11000

![Screenshot-20190610220359-741x331](https://user-images.githubusercontent.com/26026993/59226778-aab32580-8bcb-11e9-97b9-166b6f24cc68.png)

8. Compiling the app and generating bootloader:

```  
make clean && make erase_flash && make -j4 && make bootloader
```

![Screenshot-20190610211147-785x771](https://user-images.githubusercontent.com/26026993/59223664-62443980-8bc4-11e9-955e-6207efbeb6ae.png)

Here two binaries have been generated. bootloader.bin and bootloader-reflash-digest.bin files.

9. The generated bootloader.bin file size is 36kB:

![Screenshot-20190610220621-1426x415](https://user-images.githubusercontent.com/26026993/59226935-09789f00-8bcc-11e9-9689-9e17baa8a8eb.png)


10. Check for the generated partition tables addresses:

```  
make partition_table
```

![Screenshot-20190610212248-784x220](https://user-images.githubusercontent.com/26026993/59224282-eba83b80-8bc5-11e9-8c1a-900c0a499cee.png)


11. Signing and writing the .bin files in to the flash:

a. Signing bootloader.bin as follows with my_flash_encryption_key.bin file.

```
espsecure.py encrypt_flash_data --keyfile my_flash_encryption_key.bin --address 0x1000 -o build/bootloader/bootloader-encrypted.bin build/bootloader/bootloader.bin
```

![Screenshot-20190610211411-786x59](https://user-images.githubusercontent.com/26026993/59223781-b7804b00-8bc4-11e9-80f4-63c051b1ee1a.png)

b. Writing the encrypted bootloader to flash at 0x1000:

```  
esptool.py --port /dev/ttyUSB0 --baud 921000 write_flash 0x1000 build/bootloader/bootloader-encrypted.bin
```  

![Screenshot-20190610211707-672x294](https://user-images.githubusercontent.com/26026993/59223983-29589480-8bc5-11e9-8bb7-596c8a9c1209.png)

c. Now encrypting partitions_two_ota.bin [partition table file]:

```  
espsecure.py encrypt_flash_data --keyfile my_flash_encryption_key.bin --address 0x11000 -o build/partitions_two_ota-encrypted.bin build/partitions_two_ota.bin
```

![Screenshot-20190610220136-791x67](https://user-images.githubusercontent.com/26026993/59226624-57d96e00-8bcb-11e9-814d-1a335c1b40c3.png)

d. Writing the partitions_two_ota.bin to flash at 0x11000:

```    
esptool.py --port /dev/ttyUSB0 --baud 921000 write_flash 0x11000 build/partitions_two_ota-encrypted.bin
``` 

![Screenshot-20190610220212-672x295](https://user-images.githubusercontent.com/26026993/59226655-6cb60180-8bcb-11e9-8d9b-17b1a4764b78.png)

e. Now encrypting bootloader-reflash-digest.bin:

```
espsecure.py encrypt_flash_data --keyfile my_flash_encryption_key.bin --address 0x0 -o build/bootloader/bootloader-reflah-digest-encrypted.bin build/bootloader/bootloader-reflash-digest.bin
```

f. Flashing bootloader-reflash-digest.bin at address 0x0:

```
esptool.py --port /dev/ttyUSB0 --baud 921000 write_flash 0x0 build/bootloader/bootloader-reflash-digest-encrypted.bin
```


e. Now encrypting app.bin: [It will take a while as the app size is large]:

```  
espsecure.py encrypt_flash_data --keyfile my_flash_encryption_key.bin --address 0x20000 -o build/app-encrypted.bin build/app.bin
```  

![Screenshot-20190610212649-699x55](https://user-images.githubusercontent.com/26026993/59224507-7852f980-8bc6-11e9-8183-32f795a33af1.png)

f. Writing the app-encrypted.bin to flash at 0x20000:

```  
esptool.py --port /dev/ttyUSB0 --baud 921000 write_flash 0x20000 build/app-encrypted.bin
``` 

![Screenshot-20190610212804-713x290](https://user-images.githubusercontent.com/26026993/59224574-a6d0d480-8bc6-11e9-84ed-71b6a2d36284.png)

12. Reset the esp32 and watch monitor:

```  
make monitor
```  

Cheers!!

# How to download this Repo?

```    
git clone https://gitlab.com/rhr407/keith.git
```    

