#include "common.h"


void app_main()
{
   // Initialize NVS.
   esp_err_t err = nvs_flash_init();
   if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      err = nvs_flash_init();
   }
   ESP_ERROR_CHECK( err );

   if(esp_flash_encryption_enabled()){
      printf("Flash Encryption is Enabled ----->>>>>>>>\n");
   }
   else{
      printf("Flash Encryption is NOT Enabled ------<<<<<<<<<\n");
   }


   initialise_wifi();
   xTaskCreatePinnedToCore(&aws_iot_task, "aws_iot_task", 9216, NULL, 5, NULL, 1);
   xTaskCreatePinnedToCore(&ota_task, "ota_task", 4 * 1024, NULL, 5, NULL, 1);

}
