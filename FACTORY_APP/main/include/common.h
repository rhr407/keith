#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>


#include "freertos/FreeRTOS.h"

#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"




#include "nvs.h"
#include "nvs_flash.h"
#include "esp_flash_encrypt.h"


#include "esp_event_loop.h"
#include "esp_log.h"





#include <stdbool.h>
#include "esp_wifi_types.h"


#include "freertos/queue.h"

#include "esp_types.h"
#include "soc/timer_group_struct.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"


#include "esp_http_client.h"
#include "esp_https_ota.h"


#include "esp_err.h"
#include "esp_log.h"



void aws_iot_task(void *param) ;

void initialise_wifi(void);

void ota_task(void * pvParameter);

